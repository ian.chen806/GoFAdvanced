﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    /*
     * <block> ::= BLOCK <block_ID> <command list>
     */
    public class Block : INode
    {
        private String _blockID;
        private INode _commandList;

        public void parse(IContext context)
        {
            context.skipToken("BLOCK");
            _blockID = context.currentToken();
            if (context.containsBlock(_blockID))
                Console.WriteLine("Duplcated block ID : '" + _blockID + "'");
		    else{
                context.putBlock(_blockID, this);
            }
            context.nextToken();
            _commandList = new CommandList();
            _commandList.parse(context);
        }

        public void execute()
        {
            _commandList.execute();
        }
    }
}