﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    /*
     * <prog> ::= SCRIPT <command list>
     */
    public class Script : INode
    {
        private INode _commandList;

        public void parse(IContext context)
        {
            context.skipToken("SCRIPT");
            _commandList = new CommandList();
            _commandList.parse(context);
        }

        public void execute()
        {
            _commandList.execute();
        }
    }
}
