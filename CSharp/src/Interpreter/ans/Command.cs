﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;

namespace Tw.Teddysoft.Gof.Interpreter.Ans
{
    /*
     * <command> ::= <block> | <primitive>
     */
    public class Command : INode
    {
        private INode _node;

        public void parse(IContext context)
        {
            if (context.currentToken().Equals("BLOCK"))
            {
                _node = new Block();
                _node.parse(context);
            }
            else
            {
                _node = new Primitive();
                _node.parse(context);
            }
        }

        public void execute()
        {
            // Do not execute Block unless someone CALLs the block
            if (_node is Primitive)
			    _node.execute();
        }
    }
}
