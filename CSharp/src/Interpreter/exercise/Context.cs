﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System;
using System.Collections.Generic;
using System.IO;

namespace Tw.Teddysoft.Gof.Interpreter.Exercise
{
    public class Context : IContext
    {
        private IEnumerator<String> _tokens;
        private String _currentToken;
        private IDictionary<String, INode> _blocks = new Dictionary<String, INode>();

        public Context(String filename)
        {
            IList<String> tokenList = new List<String>();
            char[] whitespace = new char[] { ' ', '\t' };
            String input = null;
                
            using (StreamReader reader = new StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read)))
            {
                while ((input = reader.ReadLine()) != null) {
                    foreach (String token in input.Trim().Split(whitespace)) {
                        tokenList.Add(token);
			        }
			    }
            }
            _tokens = tokenList.GetEnumerator();
            nextToken();
        }
	
        public String nextToken()
        {
            _currentToken = null;

            if (_tokens.MoveNext())
            {
                _currentToken = _tokens.Current;
            }
            return _currentToken;
        }

        public String currentToken()
        {
            return _currentToken;
        }

        public void skipToken(String token)
        {
            if (!token.Equals(_currentToken))
            {
                Console.WriteLine("Warning: '" + token +
                               "' is expected, but '" +
                               _currentToken + "' is found.");
            }
            nextToken();
        }

        public bool containsBlock(String blockID)
        {
            return _blocks.ContainsKey(blockID);
        }

        public void putBlock(String blockID, INode blockNode)
        {
            _blocks.Add(blockID, blockNode);
        }

        public INode getBlock(String blockID)
        {
            if (_blocks.ContainsKey(blockID))
                return _blocks[blockID];
            else
                return null;
        }
    }
}
