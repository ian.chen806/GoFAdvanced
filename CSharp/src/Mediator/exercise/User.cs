﻿
/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public class User : IAgent
    {
        private string id;
        private BoardChannel notifyBoardChannel;

        public User(string id, string boardId, BoardChannel notifyBoardChannel)
        {
            this.id = id;
            this.notifyBoardChannel = notifyBoardChannel;
            this.notifyBoardChannel.Join(boardId, this);
        }

        public void FireEvent(DomainEvent e, string boardId)
        {
            notifyBoardChannel.BroadcastEvent(e, boardId, this);
        }

        public void ReceiveEvent(DomainEvent e)
        {
            if (e is CardEvents.CardMoved cardMoved)
            {
                Console.WriteLine($"{id} received event from {cardMoved.UserId}");
            }
        }

        public string GetId()
        {
            return id;
        }
    }

}

