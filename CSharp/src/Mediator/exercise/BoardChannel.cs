﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public interface BoardChannel
    {
        void Join(string boardId, IAgent agent);

        void Left(string boardId, IAgent agent);

        void BroadcastEvent(DomainEvent domainEvent, string boardId, IAgent agent);
    }

}
