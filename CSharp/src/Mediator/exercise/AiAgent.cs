﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using static Tw.Teddysoft.Gof.Mediator.Ans.CardEvents;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public class AiAgent : IAgent
    {
        private string id;
        private BoardChannel notifyBoardChannel;

        public AiAgent(string id, string boardId, BoardChannel notifyBoardChannel)
        {
            this.id = id;
            this.notifyBoardChannel = notifyBoardChannel;
            this.notifyBoardChannel.Join(boardId, this);
        }

        public string GetId()
        {
            return id;
        }

        public void FireEvent(DomainEvent domainEvent, string boardId)
        {
            notifyBoardChannel.BroadcastEvent(domainEvent, boardId, this);
        }

        public void LogEvent(DomainEvent domainEvent)
        {
            switch (domainEvent)
            {
                case CardEvents.CardMoved cardMovedEvent:
                    Console.WriteLine($"{id} received event from {cardMovedEvent.UserId}");
                    break;
                default:
                    break;
            }
        }
    }


}
