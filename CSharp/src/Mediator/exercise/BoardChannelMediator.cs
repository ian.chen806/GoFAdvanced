﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Mediator.Exercise;

namespace TW.Teddysoft.Mediator.exercise;

public class BoardChannelMediator : BoardChannel
{
    private readonly Dictionary<string, List<IAgent>> _map = new();

    public void Join(string boardId, IAgent agent)
    {
        if (!_map.TryGetValue(boardId, out var list))
        {
            list = new List<IAgent>();
            _map.Add(boardId, list);
        }

        list.Add(agent);
    }

    public void Left(string boardId, IAgent agent)
    {
        if (_map.TryGetValue(boardId, out var list))
        {
            list.Remove(agent);
        }
    }

    public void BroadcastEvent(DomainEvent domainEvent, string boardId, IAgent agent)
    {
        if (!_map.TryGetValue(boardId, out var list))
        {
            return;
        }

        foreach (var item in list.Where(r => r.GetId() != agent.GetId()))
        {
            switch (item)
            {
                case User user:
                    user.ReceiveEvent(domainEvent);
                    break;

                case AiAgent ai:
                    ai.LogEvent(domainEvent);
                    break;
            }
        }
    }
}