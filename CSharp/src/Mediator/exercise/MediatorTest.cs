﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.IO;
using NUnit.Framework;
using TW.Teddysoft.Mediator.exercise;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    [TestFixture]
    public class BoardChannelMediatorTest
    {
        [Test]
        public void TestMediator()
        {
            var output = new StringWriter();
            Console.SetOut(output);

            BoardChannel mediator = new BoardChannelMediator();
            var boardId = Guid.NewGuid().ToString();
            IAgent teddy = new User("Teddy", boardId, mediator);
            _ = new User("Ada", boardId, mediator);
            _ = new User("Eiffel", boardId, mediator);
            _ = new User("Pascal", boardId, mediator);
            _ = new AiAgent("ChatPPT", boardId, mediator);

            teddy.FireEvent(CardEvent(teddy), boardId);
            teddy.FireEvent(BoardEvent(teddy), boardId);

            var expected =
                "Ada received event from Teddy\r\nEiffel received event from Teddy\r\nPascal received event from Teddy\r\nChatPPT received event from Teddy";

            Assert.AreEqual(expected.Trim(), output.ToString().Trim());
        }

        private static BoardEvents.BoardCreated BoardEvent(IAgent teddy)
        {
            return new BoardEvents.BoardCreated(
                "board id",
                "Scrum Board",
                teddy.GetId(),
                Guid.NewGuid(),
                DateTime.Now);
        }

        private static CardEvents.CardMoved CardEvent(IAgent teddy)
        {
            return new CardEvents.CardMoved(
                "scrum_board-888",
                "to_do",
                "doing",
                "card001",
                teddy.GetId(),
                Guid.NewGuid(),
                DateTime.Now);
        }
    }
}