﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Mediator.Exercise
{
    public interface IAgent
    {
        string GetId();
        void FireEvent(DomainEvent @event, string boardId);
    }

}
