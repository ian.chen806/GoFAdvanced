﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Proxy.Exercise;

[TestFixture]
public class UiDescriptionManagerTest
{
    [Test]
    public void testUseRealObjectToLoadUIDescription()
    {
        UIDescriptionManager manager = new UIDescriptionManagerImpl();
        var start = DateTime.Now;
        manager.getDescription("ID_LIST_PRODUCT");
        manager.getDescription("ID_LIST_PRODUCT");
        manager.getDescription("ID_LIST_PRODUCT");
        var end = DateTime.Now;
        var ts = end - start;

        Assert.True(6000 < ts.TotalMilliseconds);
    }

    [Test]
    public void testUseProxyToLoadUIDescription()
    {
        UIDescriptionManager manager = new UIDescriptionManagerProxy(new UIDescriptionManagerImpl());
        var start = DateTime.Now;
        manager.getDescription("ID_LIST_PRODUCT");
        manager.getDescription("ID_LIST_PRODUCT");
        manager.getDescription("ID_LIST_PRODUCT");
        var end = DateTime.Now;
        var ts = end - start;

        Assert.True(3000 > ts.TotalMilliseconds);
    }
}