﻿using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Proxy.Exercise;

public class UIDescriptionManagerProxy : UIDescriptionManager
{
    private readonly UIDescriptionManagerImpl _manager;
    private Dictionary<string, string> _cache;

    public UIDescriptionManagerProxy(UIDescriptionManagerImpl manager)
    {
        _manager = manager;
        _cache = new Dictionary<string, string>();
    }

    public string getDescription(string aID)
    {
        if (_cache.TryGetValue(aID, out string result))
        {
            return result;
        }

        var description = _manager.getDescription(aID);
        _cache.Add(aID, description);
        return description;
    }
}