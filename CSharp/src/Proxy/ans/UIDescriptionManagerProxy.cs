﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Proxy.Ans
{
    public class UIDescriptionManagerProxy : UIDescriptionManager
    {
        private UIDescriptionManager source;
        private IDictionary<String, String> map;

        public UIDescriptionManagerProxy(UIDescriptionManager aSource)
        {
            source = aSource;
            map = new Dictionary<String, String>();
        }

        public String getDescription(String aID)
        {
            if (!isDescriptionCached(aID)) {
                cacheDescription(aID, source.getDescription(aID));
            }
            return getCachedDescription(aID);
        }

        private bool isDescriptionCached(String aID)
        {
            if (map.ContainsKey(aID))
                return true;
            else
                return false;
        }

        private String getCachedDescription(String aID)
        {
            return map[aID];
        }

        private void cacheDescription(String aID, String aValue)
        {
            map.Add(aID, aValue);
        }
    }
}