﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Ans.Entity
{
    public class Result
    {
        private readonly StepExecutionOutcome executionOutcome;
        private readonly Exception error;

        public static Result Successful()
        {
            return new Result(StepExecutionOutcome.Success, null);
        }

        public static Result Failure(Exception error)
        {
            return new Result(StepExecutionOutcome.Failure, error);
        }

        public static Result Skip()
        {
            return new Result(StepExecutionOutcome.Skipped, null);
        }

        public static Result Pending(Exception error)
        {
            return new Result(StepExecutionOutcome.Pending, error);
        }

        private Result(StepExecutionOutcome outcome, Exception error)
        {
            this.executionOutcome = outcome;
            this.error = error;
        }

        public override string ToString()
        {
            return executionOutcome.ToString();
        }

        public Exception GetException()
        {
            return error;
        }
    }
}
