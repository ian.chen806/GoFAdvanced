﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.IO;
using System.Text;
using Tw.Teddysoft.Gof.Visitor.Exercise.Entity;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Visitor
{
    public class PlainTextVisitor : SpecificationElementVisitor
    {
        private readonly StringBuilder output = new StringBuilder();

        public void Visit(ISpecificationElement element)
        {
            switch (element)
            {
                case Feature feature:
                    Visit(feature);
                    break;

                case Scenario scenario:
                    Visit(scenario);
                    break;

                case Step step:
                    Visit(step);
                    break;

                default:
                    break;
            }
        }

        private void Visit(Step step)
        {
            output.Append($"\n[{step.GetResult()}] {step.GetName()} {step.Description}");
        }

        private void Visit(Scenario scenario)
        {
            output.Append($"\n\n{scenario.FullText()}");
        }

        private void Visit(Feature feature)
        {
            output.Append(feature.FeatureText());
        }

        public string GetOutput()
        {
            return output.ToString();
        }

        public void WriteToFile(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.Write(output.ToString());
            }
        }
    }
}