﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Visitor
{
    public interface ISpecificationElement
    {
        // TODO
        //void Accept(SpecificationElementVisitor visitor);
        string GetName();

        void Accept(PlainTextVisitor visitor);
    }

}
