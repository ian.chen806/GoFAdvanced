﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using NUnit.Framework;
using Tw.Teddysoft.Gof.Visitor.Exercise.Entity;
using Tw.Teddysoft.Gof.Visitor.Exercise.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Exercise;

[TestFixture]
public class VisitorTest
{
    [Test]
    public void Test_PlainTextVisitor()
    {
        var feature = Feature.New("Visitor design pattern");

        feature.NewScenario("Simple scenario")
            .Given("the tax excluded price of a computer is $20,000", () =>
            {
            })
            .And("the VAT rate is 5%", () =>
            {
            })
            .When("I buy the computer", () =>
            {
            })
            .Then("I need to pay $21,000", () =>
            {
            }).Execute();

        var visitor = new PlainTextVisitor();
        feature.Accept(visitor);
        visitor.WriteToFile("./plaintext.txt"); // the file is in the bin/debug folder
    }
}