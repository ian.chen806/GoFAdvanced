﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;
using Tw.Teddysoft.Gof.Visitor.Exercise.Visitor;

namespace Tw.Teddysoft.Gof.Visitor.Exercise.Entity
{
    public class Scenario : ISpecificationElement
    {
        public static readonly string KEYWORD = "Scenario";
        private readonly string name;
        private readonly List<Step> steps;

        public Scenario(string name)
        {
            this.name = name;
            steps = new List<Step>();
        }

        public string GetName()
        {
            return name;
        }

        public void Accept(PlainTextVisitor visitor)
        {
            visitor.Visit(this);
            foreach (var step in this.steps)
            {
                visitor.Visit(step);
            }
        }

        public Scenario Given(string description, Action callback)
        {
            var step = new Given(description, callback);
            Steps().Add(step);
            return this;
        }

        public Scenario When(string description, Action callback)
        {
            var step = new When(description, callback);
            Steps().Add(step);
            return this;
        }

        public Scenario Then(string description, Action callback)
        {
            var step = new Then(description, callback);
            Steps().Add(step);
            return this;
        }

        public Scenario And(string description, Action callback)
        {
            var step = new And(description, callback);
            Steps().Add(step);
            return this;
        }

        public List<Step> Steps()
        {
            return steps;
        }

        public string FullText()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Scenario: ").Append(GetName()).Append("\n");

            foreach (var each in Steps())
            {
                sb.Append(each.GetName());
                if (each.Description.Length != 0)
                {
                    sb.Append(" ").Append(each.Description);
                }

                sb.Append("\n");
            }

            return sb.ToString();
        }

        protected void ExecuteStep(Scenario self, Step step)
        {
            try
            {
                step.Callback.Invoke();
                step.SetResult(Result.Successful());
            }
            catch (Exception e)
            {
                step.SetResult(Result.Failure(e));
                throw e;
            }
        }

        public void Execute()
        {
            for (int i = 0; i < Steps().Count; i++)
            {
                try
                {
                    ExecuteStep(this, Steps()[i]);
                }
                catch (Exception e)
                {
                    for (int k = i + 1; k < Steps().Count; k++)
                    {
                        Steps()[k].SetResult(Result.Skip());
                    }

                    throw e;
                }
            }
        }
    }
}