﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.Prototype.Exercise;

[TestFixture]
public class MonsterTest
{
    [Test]
    public void testCreateMonstersAfterApplyingPrototype()
    {
        var max = 50;
        var start = DateTime.Now;
        var monster = new Monster(100, 100, "./monster.xml");

        for (var i = 0; i < max; i++)
        {
            var clone = monster.CloneObject();
        }

        var end = DateTime.Now;
        var ts = end - start;

        Console.WriteLine("Create " + max + " Monster needs: " + ts.TotalMilliseconds + " ms");
    }

    [Test]
    public void testMasterBeforePrototype()
    {
        var monster = new Monster(100, 100, "/master.xml");

        Console.WriteLine("Original Master...");
        monster.display();

        Console.WriteLine("Master was hit.");
        monster.hit();
        var clone1 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());
        var clone2 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());

        Console.WriteLine("Clone two master, clone1 and clone 2.");

        Console.WriteLine("Original Master...");
        monster.display();
        Console.WriteLine("Clone 1...");
        clone1.display();
        Console.WriteLine("Clone 2...");
        clone2.display();

        Console.WriteLine("Master was hit twice.");
        monster.hit();
        monster.hit();

        Console.WriteLine("Clone one master, clone3.");
        var clone3 = new Monster(monster.getHP(), monster.getPower(), monster.getExterior().getSource());

        Console.WriteLine("Original Master...");
        monster.display();
        Console.WriteLine("Clone 1...");
        clone1.display();
        Console.WriteLine("Clone 2...");
        clone2.display();
        Console.WriteLine("Clone 3...");
        clone3.display();
    }

    [Test]
    public void testMasterClone()
    {
        var master = new Monster(100, 100, "/master.xml");
        Console.WriteLine("Original Master...");
        master.display();

        Console.WriteLine("Master was hit.");
        master.hit();
        //TODO
        var clone1 = master.CloneObject();
        var clone2 = master.CloneObject();

        Console.WriteLine("Clone two master, clone1 and clone 2.");

        Console.WriteLine("Original Master...");
        master.display();
        Console.WriteLine("Clone 1...");
        clone1.display();
        Console.WriteLine("Clone 2...");
        clone2.display();

        Console.WriteLine("Master was hit twice.");
        master.hit();
        master.hit();

        Console.WriteLine("Clone one master, clone3.");
        var clone3 = master.CloneObject();

        Console.WriteLine("Original Master...");
        master.display();
        Console.WriteLine("Clone 1...");
        clone1.display();
        Console.WriteLine("Clone 2...");
        clone2.display();
        Console.WriteLine("Clone 3...");
        clone3.display();
    }
}