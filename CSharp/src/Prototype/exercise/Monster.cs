﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Prototype.Exercise
{
    public class Monster : ICloneable
    {
        private int hp = 100;
        private int power = 50;
        private Exterior ext = null;

        public Monster(int hp, int power, String aFileName)
        {
            this.hp = hp;
            this.power = power;
            this.ext = new Exterior(aFileName);
        }

        public int getHP()
        {
            return hp;
        }

        public int getPower()
        {
            return power;
        }

        public Exterior getExterior()
        {
            return ext;
        }

        public void display()
        {
            Console.WriteLine("HP: " + hp + ", Power: " + power + "\n");
        }

        public void hit()
        {
            this.hp -= 10;
            this.power += 5;
        }

        public object Clone()
        {
            return new Monster(hp, power, ext.getSource());
        }

        public Monster CloneObject()
        {
            return Clone() as Monster;
        }
    }
}