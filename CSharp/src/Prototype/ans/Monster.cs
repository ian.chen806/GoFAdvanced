﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Prototype.Ans
{
    public class Monster
    {
        private int hp = 100;
        private int power = 50;
        Exterior ext = null;

        public Monster() { }

        public Monster clone()
        {
            Monster newObject = new Monster();
            newObject.hp = this.hp;
            newObject.power = this.power;
            newObject.ext = this.ext.clone();
            return newObject;
        }

        public Monster cloneShallow()
        {
            Monster newObject = null;
            newObject.hp = this.hp;
            newObject.power = this.power;
            newObject.ext = this.ext;
            return newObject;
        }

        public Monster(int _hp, int _power, String aFileName)
        {
            this.hp = _hp;
            this.power = _power;
            this.ext = new Exterior(aFileName);
        }

        public int getHP()
        {
            return hp;
        }

        public int getPower()
        {
            return power;
        }

        public Exterior getExterior()
        {
            return ext;
        }

        public void display()
        {
            Console.WriteLine("HP: " + hp + ", Power: " + power + "\n");
        }

        public void hit()
        {
            this.hp -= 10;
            this.power += 5;
        }
    }
}
