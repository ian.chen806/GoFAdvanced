﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Prototype.Ans
{
    public class Exterior
    {
        public String _source = null;

        public Exterior clone()
        {
            Exterior newObject = null;
            memoryCopy(this, newObject);
            return newObject;
        }

        public Exterior(String aFileName)
        {
            _source = aFileName;
            // loading exterior from the file needs 20 ms
            Thread.Sleep(20);
        }

        public String getSource()
        {
            return _source;
        }

        private void memoryCopy(Exterior aSrource, Exterior aDest)
        {
            // memroy copy needs 5 ms
            Thread.Sleep(5);
        }
    }
}
