﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Iterator.Ans
{
    public class BookList : Aggregatable
    {
        private IList<Book> list;

        public BookList()
        {
            list = new List<Book>();
        }

        public Iterator createIterator()
        {
            return new BookIterator(this);
        }

        public void addBook(Book aBook)
        {
            list.Add(aBook);
        }

        public int size()
        {
            return list.Count;
        }

        public bool isEmpty()
        {
            return (list.Count == 0);
        }

        private class BookIterator : Iterator
        {
            private int position = 0;
            private BookList parent;

            public BookIterator(BookList booklist)
            {
                parent = booklist;
            }

            public bool hasNext()
            {
                if (position < parent.list.Count)
                    return true;
                else
                    return false;
            }

            public Object next()
            {
                if (this.hasNext())
                    return parent.list[position++];
                else
                    return new ApplicationException("End of book list");
            }
        }
    }
}
