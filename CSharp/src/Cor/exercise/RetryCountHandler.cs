﻿using System.Text;
using Tw.Teddysoft.Gof.Cor.Exercise;

namespace TW.Teddysoft.Cor.exercise;

public class RetryCountHandler : ServiceHandler
{
    protected override void InternalHandler(Service service, StringBuilder result)
    {
        if (service.getRetry() >= 5)
        {
            // auto adjust the check interval
            result.Append("Too many attempts please try again later.\n");
        }
    }
}