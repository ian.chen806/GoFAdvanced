﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Exercise
{
    public class Monitor
    {

        public String handleServiceCheck(Service service)
        {
            StringBuilder result = new StringBuilder();

            switch (service.getType())
            {
                case ServiceType.Database:
                    checkRetry(service, result);
                    checkCriticalDuration(service, result);
                    break;
                case ServiceType.UserDefined:
                    checkRetry(service, result);
                    checkCriticalDuration(service, result);
                    checkStateChange(service, result);
                    break;
                default:
                    break;
            }

            return result.ToString();
        }

        private void checkRetry(Service service, StringBuilder result)
        {
            if (service.getRetry() >= 5)
            {
                // auto adjust the check interval
                result.Append("Too many attempts please try again later.\n");
            }
        }

        private void checkCriticalDuration(Service service, StringBuilder result)
        {
            if (State.Critical == service.getCurrentState() &&
                    afterOneDay(service.getLastStateChange()))
            {
                // send an email to the manager
                result.Append("Escalation due to critical state over one day.\n");
            }
        }

        private void checkStateChange(Service service, StringBuilder result)
        {
            if (service.IsStateChanged())
            {
                result.Append("State change from " + service.getPreviousState() +
                        " to " + service.getCurrentState() + ".\n");
            }
        }

        private bool afterOneDay(DateTime date)
        {
            return date < DateTime.Now ? true : false;
        }
    }
}
