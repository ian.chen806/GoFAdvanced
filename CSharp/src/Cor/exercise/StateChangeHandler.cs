﻿using System.Text;
using Tw.Teddysoft.Gof.Cor.Exercise;

namespace TW.Teddysoft.Cor.exercise;

public class StateChangeHandler : ServiceHandler
{
    protected override void InternalHandler(Service service, StringBuilder result)
    {
        if (service.IsStateChanged())
        {
            result.Append("State change from " + service.getPreviousState() +
                " to " + service.getCurrentState() + ".\n");
        }
    }
}