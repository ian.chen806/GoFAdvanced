﻿using System.Text;
using Tw.Teddysoft.Gof.Cor.Exercise;

namespace TW.Teddysoft.Cor.exercise;

public abstract class ServiceHandler
{
    private ServiceHandler _handler;

    public void Handle(Service service, StringBuilder result)
    {
        InternalHandler(service, result);
        _handler?.Handle(service, result);
    }

    public void SetNext(ServiceHandler handler)
    {
        _handler = handler;
    }

    protected abstract void InternalHandler(Service service, StringBuilder result);
}