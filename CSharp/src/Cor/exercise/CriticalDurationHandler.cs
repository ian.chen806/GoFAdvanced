﻿using System;
using System.Text;
using Tw.Teddysoft.Gof.Cor.Exercise;

namespace TW.Teddysoft.Cor.exercise;

public class CriticalDurationHandler : ServiceHandler
{
    protected override void InternalHandler(Service service, StringBuilder result)
    {
        if (State.Critical == service.getCurrentState() &&
            AfterOneDay(service.getLastStateChange()))
        {
            result.Append("Escalation due to critical state over one day.\n");
        }
    }

    private bool AfterOneDay(DateTime date)
    {
        return date < DateTime.Now;
    }
}