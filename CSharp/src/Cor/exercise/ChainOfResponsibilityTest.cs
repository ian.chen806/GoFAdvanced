﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;
using NUnit.Framework;
using TW.Teddysoft.Cor.exercise;

namespace Tw.Teddysoft.Gof.Cor.Exercise;

[TestFixture]
public class ChainOfResponsibilityTest
{
    private Service _mySqlCritical;
    private StringBuilder _result;
    private Service _userDefinedCritical;
    private Service _userDefinedOk;

    [SetUp]
    public void setup()
    {
        _mySqlCritical = new Service(ServiceType.Database, "MySQL 6.5");
        _userDefinedCritical = new Service(ServiceType.UserDefined, "My Microservice");
        _userDefinedOk = new Service(ServiceType.UserDefined, "My Microservice");
        _result = new StringBuilder();

        for (var i = 0; i < 10; i++)
        {
            _mySqlCritical.incRetry();
            _userDefinedCritical.incRetry();
        }

        var date = new DateTime(2017, 7, 19);
        _mySqlCritical.setCurrentState(State.Critical, date);
        _userDefinedCritical.setCurrentState(State.Critical, date);
        _userDefinedOk.setCurrentState(State.Ok, date);
    }

    [Test]
    public void when_too_many_attepmts_and_critical_state_over_one_day_for_database_service()
    {
        var expectedResult = "Too many attempts please try again later.\n" +
            "Escalation due to critical state over one day.\n";

        ServiceHandler retryCountHandler = new RetryCountHandler();
        ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
        retryCountHandler.SetNext(criticalDurationHandler);

        retryCountHandler.Handle(_mySqlCritical, _result);
        Assert.AreEqual(expectedResult, _result.ToString());
    }

    [Test]
    public void when_too_many_attepmts_and_critical_state_over_one_day_for_userdefined_serivce()
    {
        var expectedResult = "Too many attempts please try again later.\n" +
            "Escalation due to critical state over one day.\n" +
            "State change from Pending to Critical.\n";

        ServiceHandler retryCountHandler = new RetryCountHandler();
        ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
        ServiceHandler stateChangeHandler = new StateChangeHandler();

        retryCountHandler.SetNext(criticalDurationHandler);
        criticalDurationHandler.SetNext(stateChangeHandler);

        retryCountHandler.Handle(_userDefinedCritical, _result);
        Assert.AreEqual(expectedResult, _result.ToString());
    }

    [Test]
    public void when_too_many_attepmts_and_Ok_state_over_one_day_for_userdefined_serivce_status()
    {
        var expectedResult = "State change from Pending to Ok.\n";

        ServiceHandler retryCountHandler = new RetryCountHandler();
        ServiceHandler criticalDurationHandler = new CriticalDurationHandler();
        ServiceHandler stateChangeHandler = new StateChangeHandler();

        retryCountHandler.SetNext(criticalDurationHandler);
        criticalDurationHandler.SetNext(stateChangeHandler);

        retryCountHandler.Handle(_userDefinedOk, _result);
        Assert.AreEqual(expectedResult, _result.ToString());
    }
}