﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Cor.Ans
{
    public enum ServiceType
    {
        Database,
        Http,
        Email,
        Ftp,
        UserDefined
    }
}
