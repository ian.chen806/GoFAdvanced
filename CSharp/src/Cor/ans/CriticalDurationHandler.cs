﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Ans
{
    public class CriticalDurationHandler : ServiceHandler
    {
        protected override void internalHandler(Service service, StringBuilder result)
        {
            if (State.Critical == service.getCurrentState() &&
                    afterOneDay(service.getLastStateChange()))
            {
                // send an email to the manager
                result.Append("Escalation due to critical state over one day.\n");
            }
        }

        private bool afterOneDay(DateTime date)
        {
            return date < DateTime.Now ? true : false;
        }
    }
}
