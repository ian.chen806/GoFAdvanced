﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;

namespace Tw.Teddysoft.Gof.Cor.Ans
{
    public abstract class ServiceHandler
    {
        private ServiceHandler next = null;

        public void handle(Service service, StringBuilder result)
        {
            internalHandler(service, result);

            if (null != next)
                next.handle(service, result);
        }

        public void setNext(ServiceHandler handler)
        {
            next = handler;
        }

        protected abstract void internalHandler(Service service,
                StringBuilder result);
    }
}
