﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Memento.Exercise.Entity
{
    public record CardEvents(string CardId, Guid Id, DateTime OccurredOn) : DomainEvent
    {
        ///////////////////////////////////////////////////////////////////////////
        public record CardCreated(            
            string LaneId,
            string CardId,
            string Description,
            DateTime? Deadline,
            Guid Id,
            DateTime OccurredOn
        ) : CardEvents(CardId, Id, OccurredOn);

        ///////////////////////////////////////////////////////////////////////////

        public record CardAssigned(
            string CardId,
            string Assignee,
            Guid Id,
            DateTime OccurredOn
        ) : CardEvents(CardId, Id, OccurredOn);

        ///////////////////////////////////////////////////////////////////////////

        public record CardUnassigned(
            string CardId,
            string Unassignee,
            Guid Id,
            DateTime OccurredOn
        ) : CardEvents(CardId, Id, OccurredOn);

        ///////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////

        public record CardDescriptionChanged(
            string CardId,
            string Description,
            Guid Id,
            DateTime OccurredOn
        ) : CardEvents(CardId, Id, OccurredOn);

        ///////////////////////////////////////////////////////////////////////////

        public record CardDeadlineChanged(
            string CardId,
            DateTime Deadline,
            Guid Id,
            DateTime OccurredOn
        ) : CardEvents(CardId, Id, OccurredOn);

        ///////////////////////////////////////////////////////////////////////////
    }
}
