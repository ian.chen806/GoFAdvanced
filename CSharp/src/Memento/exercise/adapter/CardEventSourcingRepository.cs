﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Newtonsoft.Json;
using Tw.Teddysoft.Gof.Memento.Exercise.Entity;
using Tw.Teddysoft.Gof.Memento.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Memento.Exercise.Adapter;

public class CardEventSourcingRepository : Repository<Card>
{
        private readonly CardStore cardStore;
        private int snapshotIncrement = 5;

        public CardEventSourcingRepository(CardStore cardStore)
        {
            this.cardStore = cardStore;
        }

        public void Save(Card card)
        {
            cardStore.Save(card);
            var snapshotEvent = cardStore.GetLastEventFromStream(GetSnapshottedStreamName(card.Id));

            if (snapshotEvent is Snapshotted snapshotted)
            {
                if (card.Version - snapshotted.Version >= snapshotIncrement)
                {
                    SaveSnapshot(card);
                }
            }
            else if (card.Version >= snapshotIncrement)
            {
                SaveSnapshot(card);
            }
            card.ClearDomainEvents();
        }

        public Card? FindById(string cardId)
        {
            var domainEvent = cardStore.GetLastEventFromStream(GetSnapshottedStreamName(cardId));
            if (null == domainEvent)
            {
                return cardStore.FindById(cardId);
            }

            var snapshotted = (Snapshotted) domainEvent;
            var card = Card.FromSnapshot(JsonConvert.DeserializeObject<Card.CardSnapshot>(snapshotted.Snapshot));
            var events = cardStore.GetEventFromStream(card.Id, card.Version);
            events.ForEach(x => card.Apply(x));
            return card;
        }

        private void SaveSnapshot(Card card)
        {
            var snapshot = card.GetSnapshot();
            var snapshotted = new Snapshotted(card.Id, JsonConvert.SerializeObject(snapshot),
                card.Version, Guid.NewGuid(), DateTime.Now);
            cardStore.SaveEvent(GetSnapshottedStreamName(card.Id), snapshotted);
        }

        public void Delete(Card card)
        {
            cardStore.Delete(card);
        }

        public static string GetSnapshottedStreamName(string cardId)
        {
            return $"Snapshot-Card-{cardId}";
        }
}