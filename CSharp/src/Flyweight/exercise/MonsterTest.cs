﻿/*
* Copyright 2017 TeddySoft Technology. 
* 
*/
using System.Collections.Generic;
using NUnit.Framework;
using TW.Teddysoft.Flyweight.exercise;

namespace Tw.Teddysoft.Gof.Flyweight.Exercise;

[TestFixture]
public class MonsterTest
{
    [Test]
    public void testTheNumberOfFlyweightEqualsTheKindsOfUsedMonster()
    {
        var max = 100;
        IList<Monster> momsters = new List<Monster>();
        for (var i = 0; i < max; i++)
        {
            momsters.Add(new Monster("蜘蛛精", 50, 80, "./modela.xml"));
        }

        for (var i = 0; i < max; i++)
        {
            momsters.Add(new Monster("白骨精", 40, 70, "./modelb.xml"));
        }

        Assert.AreEqual(200, momsters.Count);
        Assert.AreEqual(2, ExteriorFactory.GetInstance().GetFlyweightSize());
    }
}