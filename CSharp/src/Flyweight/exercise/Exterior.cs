﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Threading;

namespace Tw.Teddysoft.Gof.Flyweight.Exercise;

public class Exterior
{
    private Color _color = null;
    private ThreeDModel _model = null;
    private readonly string _source;
    private Style _style = null;

    public Exterior(string aFileName)
    {
        _source = aFileName;

        // loading exterior from the file needs 20 ms
        Thread.Sleep(20);
    }

    public Exterior Clone()
    {
        Exterior newObject = null;
        MemoryCopy(this, newObject);
        return newObject;
    }

    public void Draw()
    {
        /* 
          use the 3D model from the _source, 
          Color, and Style to show the 3D model on the screen.		
        */
    }

    public double GetFlyweightSize()
    {
        throw new NotImplementedException();
    }

    public string GetSource()
    {
        return _source;
    }

    private void MemoryCopy(Exterior aSrource, Exterior aDest)
    {
        // copy exterior from memory needs 5 ms
        Thread.Sleep(5);
    }
}