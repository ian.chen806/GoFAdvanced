﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Tw.Teddysoft.Gof.Flyweight.Exercise;

public class ExteriorFactory
{
    private static readonly ExteriorFactory _instance = new();
    private readonly IDictionary<string, Exterior> _pool = new Dictionary<string, Exterior>();

    public static ExteriorFactory GetInstance()
    {
        return _instance;
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public Exterior GetFlyweight(string aFileName)
    {
        if (null == aFileName)
        {
            throw new ApplicationException("File name cannot be null.");
        }

        if (!_pool.ContainsKey(aFileName))
        {
            _pool.Add(aFileName, new SharedExterior(aFileName));
        }

        return _pool[aFileName];
    }

    public int GetFlyweightSize()
    {
        return _pool.Count;
    }
}