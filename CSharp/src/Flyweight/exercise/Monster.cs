﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using Tw.Teddysoft.Gof.Flyweight.Exercise;

namespace TW.Teddysoft.Flyweight.exercise;

public class Monster
{
    private readonly Color _color = null;
    private readonly Style _style = null;
    private Exterior _exterior;
    private int _hp = 100;
    private string _name;
    private int _power = 50;

    public Monster(string aName, int _hp, int _power, string aFileName)
    {
        _name = aName;
        this._hp = _hp;
        this._power = _power;

        _exterior = ExteriorFactory.GetInstance().GetFlyweight(aFileName);
    }

    public Monster CloneShallow()
    {
        Monster newObject = null;
        newObject._name = _name;
        newObject._hp = _hp;
        newObject._power = _power;
        newObject._exterior = _exterior;
        return newObject;
    }

    public void Display()
    {
        Console.WriteLine("Name: " + _name + ", HP: " + _hp + ", Power: " + _power + "\n");
    }

    public Exterior GetExterior()
    {
        return _exterior;
    }

    public int GetHP()
    {
        return _hp;
    }

    public int GetPower()
    {
        return _power;
    }

    public void Hit()
    {
        _hp -= 10;
        _power += 5;
    }
}