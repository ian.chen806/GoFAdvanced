﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.Flyweight.Ans
{
    public class Monster
    {
        private int _hp = 100;
        private int _power = 50;
        private String _name;
        private Exterior _exterior = null;

        private Color _color = null;
        private Style _style = null;

        public Monster(String aName, int _hp, int _power, String aFileName)
        {
            this._name = aName;
            this._hp = _hp;
            this._power = _power;

            this._exterior = ExteriorFactory.getInstance().getFlyweight(aFileName);
        }

        public Monster cloneShallow()
        {
            Monster newObject = null;
            newObject._name = this._name;
            newObject._hp = this._hp;
            newObject._power = this._power;
            newObject._exterior = this._exterior;
            return newObject;
        }

        public int getHP()
        {
            return _hp;
        }

        public int getPower()
        {
            return _power;
        }

        public Exterior getExterior()
        {
            return _exterior;
        }

        public void display()
        {
            Console.WriteLine("Name: " + _name + ", HP: " + _hp + ", Power: " + _power + "\n");
        }

        public void hit()
        {
            this._hp -= 10;
            this._power += 5;
        }
    }
}
