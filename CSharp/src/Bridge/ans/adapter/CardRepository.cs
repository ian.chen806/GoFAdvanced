﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Bridge.Ans.Entity;
using Tw.Teddysoft.Gof.Bridge.Ans.Usecase;

namespace Tw.Teddysoft.Gof.Bridge.Ans.Adapter
{
    public class CardRepository : Repository<Card, CardPo>
    {
        public CardRepository(RepositoryImpl<CardPo> implementation) : base(implementation) { }

        public override void Save(Card card)
        {
            CardPo po = new CardPo(card.Id, card.CardName, card.Assignee, new List<DomainEventPo>());
            impl.Save(po);
        }

        public override Card? FindById(string id)
        {
            CardPo? po = (CardPo) impl.FindById(id);
            
            return new Card(po.Id, po.Name, po.Assignee);
        }
    }

}

