﻿/*
 * Copyright 2017 TeddySoft Technology.
 * 
 */
using System;
using NUnit.Framework;
using System.IO;
using Tw.Teddysoft.Gof.Bridge.Ans.Adapter;
using Tw.Teddysoft.Gof.Bridge.Ans.Entity;
using Tw.Teddysoft.Gof.Bridge.Ans.Usecase;

namespace Tw.Teddysoft.Gof.Bridge.Ans
{
    [TestFixture]
    public class BridgeTest
    {

        [Test]
        public void TestInMemoryBoardRepository()
        {

            Repository<Board, BoardPo> boardRepository = new BoardRepository(new InMemoryRepositoryImpl<BoardPo>());
            string boardId = Guid.NewGuid().ToString();
            Board board = new Board(boardId, "Scrum");

            boardRepository.Save(board);

            Board storedBoard = boardRepository.FindById(boardId);
            Assert.AreEqual(boardId, storedBoard.Id);
            Assert.AreEqual("Scrum", storedBoard.BoardName);
        }

        [Test]
        public void TestFakeMySqlBoardRepository()
        {

            Repository<Board, BoardPo> boardRepository = new BoardRepository(new FakeMySQLRepositoryImpl<BoardPo>());
            string boardId = Guid.NewGuid().ToString();
            Board board = new Board(boardId, "Scrum");

            boardRepository.Save(board);

            Board storedBoard = boardRepository.FindById(boardId);
            Assert.AreEqual(boardId, storedBoard.Id);
            Assert.AreEqual("Scrum", storedBoard.BoardName);
        }

        [Test]
        public void TestInMemoryCardRepository()
        {

            Repository<Card, CardPo> cardRepository = new CardRepository(new InMemoryRepositoryImpl<CardPo>());
            string cardId = Guid.NewGuid().ToString();
            Card card = new Card(cardId, "Implement bridge pattern", "Teddy");

            cardRepository.Save(card);

            Card storedCard = cardRepository.FindById(cardId);
            Assert.AreEqual(cardId, storedCard.Id);
            Assert.AreEqual("Implement bridge pattern", storedCard.CardName);
            Assert.AreEqual("Teddy", storedCard.Assignee);
        }

        [Test]
        public void TestFakeMySqlCardRepository()
        {

            Repository<Card, CardPo> cardRepository = new CardRepository(new FakeMySQLRepositoryImpl<CardPo>());
            string cardId = Guid.NewGuid().ToString();
            Card card = new Card(cardId, "Implement bridge pattern", "Teddy");

            cardRepository.Save(card);

            Card storedCard = cardRepository.FindById(cardId);
            Assert.AreEqual(cardId, storedCard.Id);
            Assert.AreEqual("Implement bridge pattern", storedCard.CardName);
            Assert.AreEqual("Teddy", storedCard.Assignee);
        }
    }
}
