﻿/*
 * Copyright TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Bridge.Exercise.Entity;

namespace Tw.Teddysoft.Gof.Bridge.Exercise.Usecase
{
    public abstract class Repository<T, U> where T : AggregateRoot where U : PersistentObject
    {
        protected RepositoryImpl<U> impl;

        public Repository(RepositoryImpl<U> implementation)
        {
            this.impl = implementation;
        }

        public abstract void Save(T entity);

        public void Delete(T entity)
        {
            impl.Delete(entity.Id);
        }

        public abstract T? FindById(string id);
    }
}

