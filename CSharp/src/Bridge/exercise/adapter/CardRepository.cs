﻿using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Bridge.Exercise.Adapter;
using Tw.Teddysoft.Gof.Bridge.Exercise.Entity;
using Tw.Teddysoft.Gof.Bridge.Exercise.Usecase;

namespace TW.Teddysoft.Bridge.exercise.adapter;

public class CardRepository : Repository<Card, CardPo>
{
    public CardRepository(RepositoryImpl<CardPo> implementation)
        : base(implementation)
    {
    }

    public override void Save(Card entity)
    {
        impl.Save(new CardPo(entity.Id, entity.CardName, entity.Assignee, new List<DomainEventPo>()));
    }

    public override Card FindById(string id)
    {
        var card = impl.FindById(id);
        return new Card(card.Id, card.Name, card.Assignee);
    }
}