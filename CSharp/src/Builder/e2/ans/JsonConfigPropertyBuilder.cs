﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Text;

namespace Tw.Teddysoft.Gof.Builder.E2.Ans
{
    public class JsonConfigPropertyBuilder : ConfigPropertyBuilder
    {
        private String _platform = null;
        private int _timeout = -1;
        private String _location = null;

        public static ConfigPropertyBuilder newBuilder()
        {
            return new JsonConfigPropertyBuilder();
        }

        public ConfigPropertyBuilder platform(String aValue)
        {
            _platform = aValue;
            return this;
        }

        public ConfigPropertyBuilder timeout(int aValue)
        {
            _timeout = aValue;
            return this;
        }

        public ConfigPropertyBuilder location(String aPath)
        {
            _location = aPath;
            return this;
        }

        public String build()
        {
		    if (null == _location){
			    throw new ConfigurationError("The LOCATION property must be set.");
               }

            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            if (null != _platform) {
			    sb.Append(getJsonElementString("PLATFORM", _platform)).Append(",");
            }
            sb.Append(getJsonElementString("TIMEOUT", _timeout));
		    sb.Append(",").Append(getJsonElementString("LOCATION", _location));
		    sb.Append("}");
		    return sb.ToString();
	}
	
	    private String getJsonElementString(String aKey, String aValue)
        {
            return "\"" + aKey + "\":" + "\"" + aValue + "\"";
        }

        private String getJsonElementString(String aKey, int aValue)
        {
            return "\"" + aKey + "\":" + aValue;
        }    
    }
}
