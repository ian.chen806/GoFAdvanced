﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using Tw.Teddysoft.Gof.Decorator.Ans.Entity;
using Tw.Teddysoft.Gof.Decorator.Ans.Usecase;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Adapter
{
    public class CardSnapshotRepositoryDecorator : RepositoryDecorator<Card>
    {
        private readonly int _snapshotIncrement = 5;

        public CardSnapshotRepositoryDecorator(EsRepository<Card> component) : base(component)
        {}

        public override void Save(Card card)
        {
            base.Save(card);

            var snapshotEvent = component.GetLastEventFromStream(GetSnapshottedStreamName(card.Id));
            if (snapshotEvent != null && snapshotEvent is Snapshotted snapshotted)
            {
                if (card.Version - snapshotted.Version >= _snapshotIncrement)
                {
                    SaveSnapshot(card);
                }
            }
            else if (card.Version >= _snapshotIncrement)
            {
                SaveSnapshot(card);
            }
        }

        public override Card? FindById(string cardId)
        {
            var domainEvent = component.GetLastEventFromStream(GetSnapshottedStreamName(cardId));
            if (domainEvent == null)
            {
                return base.FindById(cardId);
            }

            var snapshotted = domainEvent as Snapshotted;
            var card = Card.FromSnapshot(JsonConvert.DeserializeObject<Card.CardSnapshot>(snapshotted.Snapshot));
            var events = component.GetEventsFromStream(card.Id, card.Version);
            events.ForEach(x => card.Apply(x));
            return card;
        }

        private void SaveSnapshot(Card card)
        {
            var snapshot = card.GetSnapshot();
            var snapshotted = new Snapshotted(card.Id, JsonConvert.SerializeObject(snapshot),
                                                          card.Version, Guid.NewGuid(), DateTime.Now);
            component.SaveEvent(GetSnapshottedStreamName(card.Id), snapshotted);
        }

        public static string GetSnapshottedStreamName(string cardId)
        {
            return $"Snapshot-Card-{cardId}";
        }
    }
}