﻿/*
 * Copyright Teddysoft Technology. 
 * 
 */
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Tw.Teddysoft.Gof.Decorator.Ans.Entity;
using Tw.Teddysoft.Gof.Decorator.Ans.Usecase;
using System.Collections.ObjectModel;

namespace Tw.Teddysoft.Gof.Decorator.Ans.Adapter
{

    public class CardEsRepository : EsRepository<Card>
    {
        private readonly Dictionary<string, List<DomainEvent>> stores = new Dictionary<string, List<DomainEvent>>();

        public void Save(Card entity)
        {
            if (!stores.ContainsKey(entity.Id))
            {
                stores[entity.Id] = new List<DomainEvent>();
            }

            long version = stores[entity.Id].Count + entity.GetDomainEvents().Count;
            stores[entity.Id].AddRange(entity.GetDomainEvents());
            entity.Version = version;
            entity.ClearDomainEvents();
        }

        public Card? FindById(string id)
        {
            if (!stores.ContainsKey(id)) return default;

            var events = stores[id];
            if (!events.Any()) return default;

            var card = new Card(events.ToList());
            return card;
        }

        public void Delete(Card entity)
        {
            Save(entity);
        }

        public DomainEvent? GetLastEventFromStream(string streamName)
        {
            if (!stores.ContainsKey(streamName)) return default;

            var events = stores[streamName];
            if (!events.Any()) return default;

            return events.Last();
        }

        public void SaveEvent(string streamName, DomainEvent @event)
        {
            if (!stores.ContainsKey(streamName))
            {
                stores[streamName] = new List<DomainEvent>();
            }
            stores[streamName].Add(@event);
        }

        public List<DomainEvent> GetEventsFromStream(string streamName, long version)
        {
            if (!stores.ContainsKey(streamName)) return new List<DomainEvent>();

            var events = stores[streamName];
            if (!events.Any()) return events.ToList();

            return events.Skip((int)version).ToList();
        }
    }
}