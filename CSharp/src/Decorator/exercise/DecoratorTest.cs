﻿/*
 * Copyright 2017 TeddySoft Technology. 
 *
 */
using System;
using NUnit.Framework;
using TW.Teddysoft.Decorator.exercise.adapter;
using Tw.Teddysoft.Gof.Decorator.Exercise.Adapter;
using Tw.Teddysoft.Gof.Decorator.Exercise.Entity;
using Tw.Teddysoft.Gof.Decorator.Exercise.Usecase;

namespace Tw.Teddysoft.Gof.Decorator.Exercise;

[TestFixture]
public class DecoratorTest
{
    [Test]
    public void CardEsRepository()
    {
        EsRepository<Card> cardRepository = new CardEsRepository();
        var card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
        card.Assign("Teddy");
        card.Assign("Eiffel");
        card.Assign("Ada");
        card.Assign("Pascal");
        card.ChangeDeadline(DateTime.Now);
        cardRepository.Save(card);
        card.ChangeDescription("new card description");
        card.Unassign("Teddy");
        cardRepository.Save(card);

        var storedCard = cardRepository.FindById(card.Id);
        Assert.AreEqual(card.Description, storedCard.Description);
        Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

        var snapshotted = cardRepository.GetLastEventFromStream(
            CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
        Assert.IsNull(snapshotted);
    }

    [Test]
    public void CardEsRepositoryAndSnapshotDecorator()
    {
        EsRepository<Card> cardRepository = new CardSnapshotRepositoryDecorator(new CardEsRepository());
        var card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
        card.Assign("Teddy");
        card.Assign("Eiffel");
        card.Assign("Ada");
        card.Assign("Pascal");
        card.ChangeDeadline(DateTime.Now);
        cardRepository.Save(card);
        card.ChangeDescription("new card description");
        card.Unassign("Teddy");
        cardRepository.Save(card);

        var storedCard = cardRepository.FindById(card.Id);
        Assert.AreEqual(card.Description, storedCard.Description);
        Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

        var snapshotted = (Snapshotted)cardRepository.GetLastEventFromStream(
            CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
        Console.WriteLine(snapshotted.Snapshot);
    }

    [Test]
    public void CardEsRepositoryAndMemoryCacheDecorator()
    {
        EsRepository<Card> cardRepository = new CardMemoryCacheRepositoryDecorator(new CardEsRepository());
        var card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
        card.Assign("Teddy");
        card.Assign("Eiffel");
        card.Assign("Ada");
        card.Assign("Pascal");
        card.ChangeDeadline(DateTime.Now);
        cardRepository.Save(card);
        card.ChangeDescription("new card description");
        card.Unassign("Teddy");
        cardRepository.Save(card);

        var storedCard = cardRepository.FindById(card.Id);
        Assert.AreEqual(card.Description, storedCard.Description);
        Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

        var snapshotted = cardRepository.GetLastEventFromStream(
            CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
        Assert.IsNull(snapshotted);
    }

    [Test]
    public void CardEsRepositoryAndMemoryCacheAndSnapshotDecorator()
    {
        EsRepository<Card> cardRepository = new CardSnapshotRepositoryDecorator(
            new CardMemoryCacheRepositoryDecorator(new CardEsRepository()));
        var card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
        card.Assign("Teddy");
        card.Assign("Eiffel");
        card.Assign("Ada");
        card.Assign("Pascal");
        card.ChangeDeadline(DateTime.Now);
        cardRepository.Save(card);
        card.ChangeDescription("new card description");
        card.Unassign("Teddy");
        cardRepository.Save(card);

        var storedCard = cardRepository.FindById(card.Id);
        Assert.AreEqual(card.Description, storedCard.Description);
        Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

        var snapshotted = cardRepository.GetLastEventFromStream(
            CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
        Assert.IsNotNull(snapshotted);
        var snapshot = ((Snapshotted)snapshotted).Snapshot;
        Console.WriteLine(snapshot);
    }

    [Test]
    public void CardEsRepositoryAndSnapshotAndMemoryCacheAndDecorator()
    {
        var cardRepository = new CardSnapshotRepositoryDecorator(
            new CardMemoryCacheRepositoryDecorator(new CardEsRepository()));
        var card = new Card("lane-001", Guid.NewGuid().ToString(), "my card");
        card.Assign("Teddy");
        card.Assign("Eiffel");
        card.Assign("Ada");
        card.Assign("Pascal");
        card.ChangeDeadline(DateTime.Now);
        cardRepository.Save(card);
        card.ChangeDescription("new card description");
        card.Unassign("Teddy");
        cardRepository.Save(card);

        var storedCard = cardRepository.FindById(card.Id);
        Assert.AreEqual(card.Description, storedCard.Description);
        Assert.AreEqual(card.Assignees.Count, storedCard.Assignees.Count);

        var snapshotted = cardRepository.GetLastEventFromStream(
            CardSnapshotRepositoryDecorator.GetSnapshottedStreamName(card.Id));
        Assert.IsNotNull(snapshotted);
        var snapshot = ((Snapshotted)snapshotted).Snapshot;
        Console.WriteLine(snapshot);
    }
}