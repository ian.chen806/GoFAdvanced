﻿using System;
using System.Collections.Generic;
using Tw.Teddysoft.Gof.Decorator.Exercise.Entity;
using Tw.Teddysoft.Gof.Decorator.Exercise.Usecase;

namespace TW.Teddysoft.Decorator.exercise.adapter;

public class CardMemoryCacheRepositoryDecorator : RepositoryDecorator<Card>
{
    private readonly Dictionary<string, Card> _cache;

    public CardMemoryCacheRepositoryDecorator(EsRepository<Card> component) : base(component)
    {
        _cache = new Dictionary<string, Card>();
    }

    public override void Delete(Card card)
    {
        Component.Delete(card);
        _cache.Remove(card.Id);
    }

    public override Card? FindById(string cardId)
    {
        if (_cache.TryGetValue(cardId, out var id))
        {
            Console.WriteLine("Cache hit!");
            return id;
        }

        var card = Component.FindById(cardId);
        if (null != card)
        {
            _cache[cardId] = card;
        }

        return card;
    }

    public override void Save(Card card)
    {
        Component.Save(card);
        _cache[card.Id] = card;
    }
}