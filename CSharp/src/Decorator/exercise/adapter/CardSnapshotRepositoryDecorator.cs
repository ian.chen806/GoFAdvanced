﻿using System;
using Newtonsoft.Json;
using Tw.Teddysoft.Gof.Decorator.Exercise.Entity;
using Tw.Teddysoft.Gof.Decorator.Exercise.Usecase;

namespace TW.Teddysoft.Decorator.exercise.adapter;

public class CardSnapshotRepositoryDecorator : RepositoryDecorator<Card>
{
    private readonly int _snapshotIncrement = 5;

    public CardSnapshotRepositoryDecorator(EsRepository<Card> component)
        : base(component)
    {
    }

    public static string GetSnapshottedStreamName(string cardId)
    {
        return $"Snapshot-Card-{cardId}";
    }

    public override Card? FindById(string cardId)
    {
        var domainEvent = Component.GetLastEventFromStream(GetSnapshottedStreamName(cardId));
        if (domainEvent == null)
        {
            return base.FindById(cardId);
        }

        var snapshotted = domainEvent as Snapshotted;
        var card = Card.FromSnapshot(JsonConvert.DeserializeObject<Card.CardSnapshot>(snapshotted.Snapshot));
        var events = Component.GetEventsFromStream(card.Id, card.Version);
        events.ForEach(x => card.Apply(x));
        return card;
    }

    public override void Save(Card card)
    {
        base.Save(card);

        var snapshotEvent = Component.GetLastEventFromStream(GetSnapshottedStreamName(card.Id));
        if (snapshotEvent != null && snapshotEvent is Snapshotted snapshotted)
        {
            if (card.Version - snapshotted.Version >= _snapshotIncrement)
            {
                SaveSnapshot(card);
            }
        }
        else if (card.Version >= _snapshotIncrement)
        {
            SaveSnapshot(card);
        }
    }

    private void SaveSnapshot(Card card)
    {
        var snapshot = card.GetSnapshot();
        var snapshotted = new Snapshotted(card.Id, JsonConvert.SerializeObject(snapshot),
            card.Version, Guid.NewGuid(), DateTime.Now);
        Component.SaveEvent(GetSnapshottedStreamName(card.Id), snapshotted);
    }
}