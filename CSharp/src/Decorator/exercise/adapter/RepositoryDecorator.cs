﻿using System.Collections.Generic;
using Tw.Teddysoft.Gof.Decorator.Exercise.Entity;
using Tw.Teddysoft.Gof.Decorator.Exercise.Usecase;

namespace TW.Teddysoft.Decorator.exercise.adapter;

public abstract class RepositoryDecorator<T> : EsRepository<T>
    where T : AggregateRoot
{
    protected readonly EsRepository<T> Component;

    protected RepositoryDecorator(EsRepository<T> component)
    {
        this.Component = component;
    }

    public virtual void Delete(T entity)
    {
        Component.Delete(entity);
    }

    public virtual DomainEvent? GetLastEventFromStream(string streamName)
    {
        return Component.GetLastEventFromStream(streamName);
    }

    public virtual List<DomainEvent> GetEventsFromStream(string streamName, long version)
    {
        return Component.GetEventsFromStream(streamName, version);
    }

    public virtual T? FindById(string id)
    {
        return Component.FindById(id);
    }

    public virtual void Save(T entity)
    {
        Component.Save(entity);
    }

    public virtual void SaveEvent(string streamName, DomainEvent @event)
    {
        Component.SaveEvent(streamName, @event);
    }
}