/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.interpreter.ans;

/*
 * <prog> ::= SCRIPT <command list>
 */
public class Script implements INode {

	private INode _commandList;
	
	@Override
	public void parse(IContext context) {
		context.skipToken("SCRIPT");
		_commandList = new CommandList();
		_commandList.parse(context);
	}

	@Override
	public void execute() {
		_commandList.execute();
	}
}
