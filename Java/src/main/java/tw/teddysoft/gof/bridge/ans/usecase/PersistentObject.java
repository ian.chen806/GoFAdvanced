package tw.teddysoft.gof.bridge.ans.usecase;

import java.util.List;

public interface PersistentObject {
    String getId();
    List<DomainEventPo> getDomainEvents();
}
