package tw.teddysoft.gof.bridge.ans.usecase;

import tw.teddysoft.gof.bridge.ans.entity.AggregateRoot;

import java.util.Optional;

public abstract class Repository<T extends AggregateRoot> {
    protected RepositoryImpl impl;
    public Repository(RepositoryImpl implementation) {
        this.impl = implementation;
    }
    public abstract void save(T entity);
    public void delete(T entity) {
        impl.delete(entity.getId());
    }
    public abstract Optional<T> findById(String id);
}
