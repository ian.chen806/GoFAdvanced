package tw.teddysoft.gof.bridge.ans.entity;

public class Card extends AggregateRoot {
    private String cardName;
    private String assignee;

    public Card(String cardId, String name, String assignee) {
        super(cardId);
        this.cardName = name;
        this.assignee = assignee;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
