package tw.teddysoft.gof.bridge.ans.adapter;

import tw.teddysoft.gof.bridge.ans.usecase.PersistentObject;
import tw.teddysoft.gof.bridge.ans.usecase.RepositoryImpl;

import java.util.*;

public class InMemoryRepositoryImpl<T extends PersistentObject> implements RepositoryImpl<T> {

    private Map<String, T> entities = new HashMap<>();
    @Override
    public void save(T po) {
        entities.put(po.getId(), po);
    }

    @Override
    public void delete(String id) {
        entities.remove(id);
    }

    @Override
    public Optional<T> findById(String id) {
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public List<T> findAll() {
        return new ArrayList<>(entities.values());
    }
}
