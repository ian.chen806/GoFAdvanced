/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.exercise;

public enum State {
	Pending,
	Ok,
	Warning,
	Critical;
}
