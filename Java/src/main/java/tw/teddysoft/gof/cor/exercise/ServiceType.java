/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.exercise;

public enum ServiceType {
	Database,
	Http,
	Email,
	Ftp,
	UserDefined;
}
