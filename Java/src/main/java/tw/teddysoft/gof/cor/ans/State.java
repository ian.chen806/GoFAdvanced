/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

public enum State {
	Pending,
	Ok,
	Warning,
	Critical;
}
