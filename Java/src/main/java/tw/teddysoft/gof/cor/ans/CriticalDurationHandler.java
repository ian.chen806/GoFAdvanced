/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.cor.ans;

import java.util.Date;

public class CriticalDurationHandler extends ServiceHandler {
	
	@Override
	protected void internalHandler(Service service, StringBuilder result) {
		if(State.Critical == service.getCurrentState() && 
				afterOneDay(service.getLastStateChange())){
			// send an email to the manager
			result.append("Escalation due to critical state over one day.\n");
		}
	}
	
	private boolean afterOneDay(Date date){
		return (date.before(new Date())) ? true : false;
	}
}
