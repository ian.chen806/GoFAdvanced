package tw.teddysoft.gof.mediator.ans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardChannelMediator implements BoardChannel {

    private final Map<String, List<Agent>> sessionMap;
    public BoardChannelMediator() {
        this.sessionMap = new HashMap<>();
    }

    @Override
    public void join(String boardId, Agent agent) {
        if (!sessionMap.containsKey(boardId)) {
            sessionMap.put(boardId, new ArrayList<>());
        }
        sessionMap.get(boardId).add(agent);
    }

    @Override
    public void left(String boardId, Agent agent) {
        if (!sessionMap.containsKey(boardId)) {
            return;
        }
        sessionMap.get(boardId).removeIf( x -> x.getId().equals(agent.getId()));
    }

    @Override
    public void broadcastEvent(DomainEvent event, String boardId, Agent agent) {
        if (!sessionMap.containsKey(boardId)) {
            return;
        }

        if (!(event instanceof CardEvents)) return;
        CardEvents cardEvent = (CardEvents) event;

        for (Agent each : sessionMap.get(boardId)){
            switch (each){
                case User user -> {
                    if (!user.getId().equals(cardEvent.userId())){
                        user.receiveEvent(event);
                    }
                }
                case AiAgent aiAgent -> {
                    aiAgent.logEvent(event);
                }
                default -> {}
            }
        }
    }
}
