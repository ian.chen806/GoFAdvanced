/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.mediator.exercise;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MediatorTest {

	@Test
	public void test_mediator() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(stream);
		System.setOut(printStream);

//		BoardChannel mediator = new BoardChannelMediator();
//		String boardId = UUID.randomUUID().toString();
//		Agent teddy = new User("Teddy", boardId, mediator);
//		Agent ada = new User("Ada", boardId, mediator);
//		Agent eiffel = new User("Eiffel", boardId, mediator);
//		Agent pascal = new User("Pascal", boardId, mediator);
//		Agent chatPpt = new AiAgent("ChatPPT", boardId, mediator);

//		teddy.fireEvent(new CardEvents.CardMoved(
//				"scrum_board-888",
//				"to_do",
//				"doing",
//				"card001",
//				teddy.getId(),
//				UUID.randomUUID(),
//				new Date()), boardId);
//
//		teddy.fireEvent(new BoardEvents.BoardCreated(
//				"board id",
//				"Scrum Board",
//				teddy.getId(),
//				UUID.randomUUID(),
//				new Date()), boardId);

		String expected = """
				Ada received event from Teddy
				Eiffel received event from Teddy
				Pascal received event from Teddy
				ChatPPT received event from Teddy   
				""";
		assertEquals(expected,stream.toString());
	}
}
