/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.iterator.ans;

public interface Aggregatable {
	Iterator createIterator();
}
