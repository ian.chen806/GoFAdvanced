package tw.teddysoft.gof.memento.exercise.entity;

import java.util.Date;
import java.util.UUID;

public interface CardEvents extends DomainEvent {

    String cardId();

    default String aggregateId(){
        return cardId();
    }

    ///////////////////////////////////////////////////////////////

    record CardCreated(
            String laneId,
            String cardId,
            String description,
            Date deadline,
            UUID id,
            Date occurredOn
    ) implements CardEvents {}

    ///////////////////////////////////////////////////////////////

    record CardAssigned(
            String cardId,
            String assignee,
            UUID id,
            Date occurredOn
    ) implements CardEvents {}

    ///////////////////////////////////////////////////////////////

    record CardUnassigned(
            String cardId,
            String unassignee,
            UUID id,
            Date occurredOn
    ) implements CardEvents {}

    ///////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////

    record CardDescriptionChanged(
            String cardId,
            String description,
            UUID id,
            Date occurredOn
    ) implements CardEvents {}

    ///////////////////////////////////////////////////////////////

    record CardDeadlineChanged(
            String cardId,
            Date deadline,
            UUID id,
            Date occurredOn
    ) implements CardEvents {}

    ///////////////////////////////////////////////////////////////

 }
