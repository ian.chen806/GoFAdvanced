package tw.teddysoft.gof.memento.exercise.usecase;

import tw.teddysoft.gof.memento.exercise.entity.AggregateRoot;

import java.util.Optional;

public interface Repository<T extends AggregateRoot> {
    Optional<T> findById(String id);
    void save(T entity);
    void delete(T entity);
}
