package tw.teddysoft.gof.memento.ans;

import tw.teddysoft.gof.memento.ans.MementoTest;

public class Player {

    public void accessMemento(){
        Memento m = new Memento();
        m.getHp();
        m.getInternal();
    }

    public static class  Memento {
        private final int hp;
        private final int internal;

        public Memento(){
            hp = 10;
            internal = 5;
        }
        public int getHp(){
            return hp;
        }
        private int getInternal(){
            return internal;
        }
    }

}
