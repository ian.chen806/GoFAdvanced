package tw.teddysoft.gof.memento.ans.adapter;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import tw.teddysoft.gof.memento.ans.entity.Card;
import tw.teddysoft.gof.memento.ans.entity.DomainEvent;
import tw.teddysoft.gof.memento.ans.entity.Json;
import tw.teddysoft.gof.memento.ans.usecase.Repository;

public class CardEventSourcingRepository implements Repository<Card>  {
    private final CardStore cardStore;
    private int snapshotIncrement = 5;

    public CardEventSourcingRepository(CardStore cardStore) {
        this.cardStore = cardStore;
    }

    @Override
    public void save(Card card) {
        cardStore.save(card);
        Optional<DomainEvent> snapshotEvent =
                cardStore.getLastEventFromStream(getSnapshottedStreamName(card.getId()));
        if (snapshotEvent.isPresent()){
            DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) snapshotEvent.get();
            if (card.getVersion() - snapshotted.version() >= snapshotIncrement){
                saveSnapshot(card);
            }
        }
        else if (card.getVersion() >= snapshotIncrement){
            saveSnapshot(card);
        }
        card.clearDomainEvents();
    }

    @Override
    public Optional<Card> findById(String cardId) {
        Optional<DomainEvent> domainEvent =
                cardStore.getLastEventFromStream(getSnapshottedStreamName(cardId));
        if (domainEvent.isEmpty()){
            return cardStore.findById(cardId);
        }

        DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) domainEvent.get();
        Card card = Card.fromSnapshot(Json.readAs(snapshotted.snapshot().getBytes(), Card.CardSnapshot.class));
        var events = cardStore.getEventFromStream(card.getId(), card.getVersion());
        events.forEach( x -> card.apply(x));
        return Optional.of(card);
    }

    private void saveSnapshot (Card card){
        Card.CardSnapshot snapshot = card.getSnapshot();
        var snapshotted = new DomainEvent.Snapshotted(card.getId(), Json.asString(snapshot),
                card.getVersion(), UUID.randomUUID(), new Date());
        cardStore.saveEvent(getSnapshottedStreamName(card.getId()), snapshotted);
    }

    @Override
    public void delete(Card card) {
        cardStore.delete(card);
    }

    public static String getSnapshottedStreamName(String cardId){
        return "Snapshot-Card-" + cardId;
    }
}
