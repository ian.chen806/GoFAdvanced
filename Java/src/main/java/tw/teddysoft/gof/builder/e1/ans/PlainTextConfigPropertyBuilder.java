/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.builder.e1.ans;

import java.util.HashMap;
import java.util.LinkedHashMap;


public class PlainTextConfigPropertyBuilder implements ConfigPropertyBuilder {
	private HashMap<String, String> _table;
		
	public PlainTextConfigPropertyBuilder(){
		_table = new LinkedHashMap<>();
	}
	
	@Override
	public void platform(String aValue){
		_table.put("PLATFORM", aValue);
	}
	
	@Override
	public void timeout(int aValue){
		_table.put("TIMEOUT", String.valueOf(aValue));
	}
	
	@Override
	public void location(String aPath){
		_table.put("LOCATION", aPath);
	}
	
	@Override
	public String build() throws ConfigurationError{
		if (!_table.containsKey("LOCATION") ){
			throw new ConfigurationError("The LOCATION property must be set.");
		}
		
		StringBuilder sb = new StringBuilder();
		for(String s : _table.keySet()){
			sb.append(s).append("=").append(_table.get(s)).append("\n");
		}
		return sb.toString();
	}
}


