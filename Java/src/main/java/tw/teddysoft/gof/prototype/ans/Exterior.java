/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.prototype.ans;


public class Exterior implements Cloneable {
	public String source = null;
	
	public Exterior clone(){
		Exterior newObject = null;
		try {
			newObject = (Exterior)super.clone();
		} catch (CloneNotSupportedException e) {
			return null;	// ignoring, should not happen
		}
		
		memoryCopy(this, newObject);

		return newObject;
	}
	
	public Exterior(String aFileName){
		source = aFileName;
		
		// loading exterior from the file
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} // sleep 20 ms
		
	}
	
	public String getSource(){
		return source;
	}
	
	
	private void memoryCopy(Exterior aSrource, Exterior aDest){
		// sleep 5 ms
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
