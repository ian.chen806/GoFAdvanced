/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.prototype.ans;


public class Monster implements Cloneable {
	private int hp = 100;	
	private int power = 50;
	Exterior ext = null;

	public Monster clone(){
		Monster newObject = null;
		try {
			newObject = (Monster)super.clone();
		} catch (CloneNotSupportedException e) {
			return null; 	// ignoring, should not happen
		}
		newObject.hp = this.hp;
		newObject.power = this.power;	
		newObject.ext = this.ext.clone();
		return newObject;
	}

	public Monster cloneShallow(){
		Monster newObject = null;
		try {
			newObject = (Monster)super.clone();
		} catch (CloneNotSupportedException e) {
			return null; 	// ignoring, should not happen
		}
		newObject.hp = this.hp;
		newObject.power = this.power;	
		newObject.ext = this.ext;
		return newObject;
	}
	
	
	public Monster (int _hp, int _power, String aFileName){
		this.hp = _hp;
		this.power = _power;
		this.ext =  new Exterior(aFileName);
	}
	
	public int getHP(){
		return hp;
	}
	
	public int getPower(){
		return power;
	}
	
	public Exterior getExterior(){
		return ext;
	}
	
	public void display(){
		System.out.println("HP: " + hp + ", Power: " + power + "\n");
	}
	
	public void hit(){
		this.hp -= 10;
		this.power += 5;
	}
}
