/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.proxy.exercise;

public interface UIDescriptionManager {
	public String getDescription(String aID) throws DescriptionNotFoundException;
}
