package tw.teddysoft.gof.decorator.ans.usecase;

import tw.teddysoft.gof.decorator.ans.entity.AggregateRoot;
import tw.teddysoft.gof.decorator.ans.entity.DomainEvent;

import java.util.List;
import java.util.Optional;

public interface EsRepository<T extends AggregateRoot> {
    Optional<T> findById(String id);
    void save(T entity);
    void delete(T entity);
    Optional<DomainEvent> getLastEventFromStream(String streamName);
    void saveEvent(String streamName, DomainEvent event);
    List<DomainEvent> getEventsFromStream(String streamName, long version);
}
