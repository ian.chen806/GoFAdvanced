package tw.teddysoft.gof.decorator.ans.adapter;

import tw.teddysoft.gof.decorator.ans.entity.AggregateRoot;
import tw.teddysoft.gof.decorator.ans.entity.DomainEvent;
import tw.teddysoft.gof.decorator.ans.usecase.EsRepository;

import java.util.List;
import java.util.Optional;

public abstract class RepositoryDecorator<T extends AggregateRoot>
        implements EsRepository<T> {

    protected EsRepository<T> component;

    public RepositoryDecorator(EsRepository component) {
        this.component = component;
    }

    @Override
    public Optional<DomainEvent> getLastEventFromStream(String streamName) {
        return component.getLastEventFromStream(streamName);
    }

    @Override
    public void saveEvent(String streamName, DomainEvent event) {
        component.saveEvent(streamName, event);
    }

    @Override
    public List<DomainEvent> getEventsFromStream(String streamName, long version) {
        return component.getEventsFromStream(streamName, version);
    }
}
