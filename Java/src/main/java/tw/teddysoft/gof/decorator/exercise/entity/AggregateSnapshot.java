package tw.teddysoft.gof.decorator.exercise.entity;

public interface AggregateSnapshot<T> {
    T getSnapshot();
    void setSnapshot(T snapshot);
}
