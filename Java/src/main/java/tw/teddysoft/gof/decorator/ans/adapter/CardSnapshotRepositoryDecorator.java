package tw.teddysoft.gof.decorator.ans.adapter;

import tw.teddysoft.gof.decorator.ans.entity.Card;
import tw.teddysoft.gof.decorator.ans.entity.DomainEvent;
import tw.teddysoft.gof.decorator.ans.entity.Json;
import tw.teddysoft.gof.decorator.ans.usecase.EsRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class CardSnapshotRepositoryDecorator extends RepositoryDecorator<Card>  {
    private int snapshotIncrement = 5;

    public CardSnapshotRepositoryDecorator(EsRepository component) {
        super(component);
    }

    @Override
    public void save(Card card) {
        component.save(card);
        Optional<DomainEvent> snapshotEvent =
                component.getLastEventFromStream(getSnapshottedStreamName(card.getId()));
        if (snapshotEvent.isPresent()){
            DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) snapshotEvent.get();
            if (card.getVersion() - snapshotted.version() >= snapshotIncrement){
                saveSnapshot(card);
            }
        }
        else if (card.getVersion() >= snapshotIncrement){
            saveSnapshot(card);
        }
    }

    @Override
    public Optional<Card> findById(String cardId) {
        Optional<DomainEvent> domainEvent =
                component.getLastEventFromStream(getSnapshottedStreamName(cardId));
        if (domainEvent.isEmpty()){
            return component.findById(cardId);
        }

        DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) domainEvent.get();
        Card card = Card.fromSnapshot(Json.readAs(snapshotted.snapshot().getBytes(), Card.CardSnapshot.class));
        List<DomainEvent> events = component.getEventsFromStream(card.getId(), card.getVersion());
        events.forEach( x -> card.apply(x));
        return Optional.of(card);
    }

    private void saveSnapshot (Card card){
        Card.CardSnapshot snapshot = card.getSnapshot();
        var snapshotted = new DomainEvent.Snapshotted(card.getId(), Json.asString(snapshot),
                card.getVersion(), UUID.randomUUID(), new Date());
        component.saveEvent(getSnapshottedStreamName(card.getId()), snapshotted);
    }

    @Override
    public void delete(Card card) {
        component.delete(card);
    }

    public static String getSnapshottedStreamName(String cardId){
        return "Snapshot-Card-" + cardId;
    }
}
