/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.decorator.ans;

import org.junit.jupiter.api.Test;
import tw.teddysoft.gof.decorator.ans.adapter.CardEsRepository;
import tw.teddysoft.gof.decorator.ans.adapter.CardMemoryCacheRepositoryDecorator;
import tw.teddysoft.gof.decorator.ans.adapter.CardSnapshotRepositoryDecorator;
import tw.teddysoft.gof.decorator.ans.entity.Card;
import tw.teddysoft.gof.decorator.ans.usecase.EsRepository;
import tw.teddysoft.gof.decorator.ans.entity.DomainEvent;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DecoratorTest {

	@Test
	public void card_es_repository() {
		EsRepository<Card> cardRepository = new CardEsRepository();
		Card card = new Card("lane-001", UUID.randomUUID().toString(), "my card");
		card.assign("Teddy");
		card.assign("Eiffel");
		card.assign("Ada");
		card.assign("Pascal");
		card.changeDeadline(new Date());
		cardRepository.save(card);
		card.changeDescription("new card description");
		card.unassign("Teddy");
		cardRepository.save(card);

		Card storedCard = cardRepository.findById(card.getId()).get();
		assertEquals(card.getDescription(), storedCard.getDescription());
		assertEquals(card.getAssignees().size(), storedCard.getAssignees().size());

		var snapshotted = cardRepository.getLastEventFromStream(
				CardSnapshotRepositoryDecorator.getSnapshottedStreamName(card.getId()));
		assertTrue(snapshotted.isEmpty());
	}

	@Test
	public void card_es_repository_and_snapshot_decorator() {

		EsRepository<Card> cardRepository = new CardSnapshotRepositoryDecorator(new CardEsRepository());
		Card card = new Card("lane-001", UUID.randomUUID().toString(), "my card");
		card.assign("Teddy");
		card.assign("Eiffel");
		card.assign("Ada");
		card.assign("Pascal");
		card.changeDeadline(new Date());
		cardRepository.save(card);
		card.changeDescription("new card description");
		card.unassign("Teddy");
		cardRepository.save(card);

		Card storedCard = cardRepository.findById(card.getId()).get();
		assertEquals(card.getDescription(), storedCard.getDescription());
		assertEquals(card.getAssignees().size(), storedCard.getAssignees().size());

		DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) cardRepository.getLastEventFromStream(
				CardSnapshotRepositoryDecorator.getSnapshottedStreamName(card.getId())).get();
		System.out.println(snapshotted.snapshot());
	}

	@Test
	public void card_es_repository_and_memory_cache_decorator() {

		EsRepository<Card> cardRepository = new CardMemoryCacheRepositoryDecorator(new CardEsRepository());
		Card card = new Card("lane-001", UUID.randomUUID().toString(), "my card");
		card.assign("Teddy");
		card.assign("Eiffel");
		card.assign("Ada");
		card.assign("Pascal");
		card.changeDeadline(new Date());
		cardRepository.save(card);
		card.changeDescription("new card description");
		card.unassign("Teddy");
		cardRepository.save(card);

		Card storedCard = cardRepository.findById(card.getId()).get();
		assertEquals(card.getDescription(), storedCard.getDescription());
		assertEquals(card.getAssignees().size(), storedCard.getAssignees().size());

		var snapshotted = cardRepository.getLastEventFromStream(
				CardSnapshotRepositoryDecorator.getSnapshottedStreamName(card.getId()));
		assertTrue(snapshotted.isEmpty());
	}

	@Test
	public void card_es_repository_and_memory_cache_and_snapshot_decorator() {

		EsRepository<Card> cardRepository = new CardMemoryCacheRepositoryDecorator(
				new CardSnapshotRepositoryDecorator(new CardEsRepository()));
		Card card = new Card("lane-001", UUID.randomUUID().toString(), "my card");
		card.assign("Teddy");
		card.assign("Eiffel");
		card.assign("Ada");
		card.assign("Pascal");
		card.changeDeadline(new Date());
		cardRepository.save(card);
		card.changeDescription("new card description");
		card.unassign("Teddy");
		cardRepository.save(card);

		Card storedCard = cardRepository.findById(card.getId()).get();
		assertEquals(card.getDescription(), storedCard.getDescription());
		assertEquals(card.getAssignees().size(), storedCard.getAssignees().size());

		DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) cardRepository.getLastEventFromStream(
				CardSnapshotRepositoryDecorator.getSnapshottedStreamName(card.getId())).get();
		System.out.println(snapshotted.snapshot());
	}

	@Test
	public void card_es_repository_and_snapshot_and_memory_cache_and_decorator() {

		EsRepository<Card> cardRepository = new CardSnapshotRepositoryDecorator(new CardMemoryCacheRepositoryDecorator(new CardEsRepository()));
		Card card = new Card("lane-001", UUID.randomUUID().toString(), "my card");
		card.assign("Teddy");
		card.assign("Eiffel");
		card.assign("Ada");
		card.assign("Pascal");
		card.changeDeadline(new Date());
		cardRepository.save(card);
		card.changeDescription("new card description");
		card.unassign("Teddy");
		cardRepository.save(card);

		Card storedCard = cardRepository.findById(card.getId()).get();
		assertEquals(card.getDescription(), storedCard.getDescription());
		assertEquals(card.getAssignees().size(), storedCard.getAssignees().size());

		DomainEvent.Snapshotted snapshotted = (DomainEvent.Snapshotted) cardRepository.getLastEventFromStream(
				CardSnapshotRepositoryDecorator.getSnapshottedStreamName(card.getId())).get();
		System.out.println(snapshotted.snapshot());
	}
}




