/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.visitor.exercise;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import tw.teddysoft.gof.visitor.exercise.entity.Feature;
import tw.teddysoft.gof.visitor.exercise.visitor.PlainTextVisitor;

public class VisitorTest {

	static Feature feature;

	@BeforeAll
	public static void beforeAll() {
		feature = Feature.New("Visitor design pattern");
	}

//	@AfterAll
//	static void afterAll() {
//		if (null == feature) return;
//
//		PlainTextVisitor plainTextVisitor = new PlainTextVisitor();
//		feature.accept(plainTextVisitor);
//		plainTextVisitor.writeToFile("./target/plaintext.txt");
//	}

	@Test
	public void test_plain_text_visitor() {
		feature.newScenario("Simple scenario")
				.Given("the tax excluded price of a computer is $20,000", () -> {
				})
				.And("the VAT rate is 5%", () -> {
				})
				.When("I buy the computer", () -> {
				})
				.Then("I need to pay $21,000", () -> {
				}).Execute();
	}

}
