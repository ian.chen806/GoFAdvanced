package tw.teddysoft.gof.visitor.ans.entity;

public enum StepExecutionOutcome {
    Pending, Success, Failure, Skipped;
}
